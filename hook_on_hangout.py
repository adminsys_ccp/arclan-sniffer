#!/usr/bin/python3
#https://developers.google.com/hangouts/chat/quickstart/incoming-bot-python
from json import dumps
from httplib2 import Http
#from google-auth-httplib2 import Http
#import httplib2shim


def hook_this(areaText):
    """Hangouts Chat incoming webhook quickstart."""
    url = 'https://chat.googleapis.com/v1/spaces/AAAAD1AibPw/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=VBtHFpaiQM40QxfkbkNai0EINZXtlWwtlQ6xln5xopI%3D'
    bot_message = {
        'text' : areaText }

    message_headers = {'Content-Type': 'application/json; charset=UTF-8'}

    #http_obj = Http()
    http_obj = Http()

    response = http_obj.request(
        uri=url,
        method='POST',
        headers=message_headers,
        body=dumps(bot_message),
    )

    print(response)

if __name__ == '__main__':
    hook_this('Hello World')
