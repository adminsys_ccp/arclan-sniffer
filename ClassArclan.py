import areadatas
import requests
import time
import datetime
from os import path
import os

class Arclan:
    def __init__(self):
        self.ip_cac_entree = areadatas.arclan_in_ip
        self.ip_cac_sortie = areadatas.arclan_out_ip
        self.port_cac_entree = areadatas.arclan_in_port
        self.port_cac_sortie = areadatas.arclan_out_port
        self.logfile = areadatas.arclan_logfile

    def _arclan_debug_in (self):
        Url = 'http://'+self.ip_cac_entree+':'+self.port_cac_entree+'/debug'
        self.arclan_in_status = requests.get(Url).text
        return self.arclan_in_status

    def _arclan_debug_out (self):
        Url = 'http://'+self.ip_cac_sortie+':'+self.port_cac_sortie+'/debug'
        self.arclan_out_status = requests.get(Url).text
        return self.arclan_out_status

    def _arclan_debug (self, ip_cac, port_cac):
        Url = 'http://'+ip_cac+':'+port_cac+'/debug'
        self.arclan_out_status = requests.get(Url).text
        return self.arclan_out_status

    def _log_result(self, level, message):
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if not os.path.exists(self.logfile):
            f = open(self.logfile, "w")
            f.close
        # with open(self.logfile, '+a') as f:
        #     f.write(timestamp+" ["+level+"] "+message+"\n")
        with open(self.logfile, '+r') as f:
            content = f.read()
            f.seek(0,0)
            line = timestamp+" ["+level+"] "+message+"\n"
            f.write(line.rstrip('\r\n') + '\n' + content)


if __name__ == "__main__":
    arcset = Arclan()
    arcset._arclan_debug_in()
    arcset._arclan_debug_out()
    arcset._log_result("INFO", "DEBUG IN "+arcset.ip_cac_entree+" "+arcset.arclan_in_status)
    arcset._log_result("INFO", "DEBUG OUT "+arcset.ip_cac_sortie+" "+arcset.arclan_out_status)


