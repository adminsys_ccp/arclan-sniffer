from ClassArclan import Arclan
import time
from hook_on_hangout import hook_this
#from httplib2 import Http


# Nombre d'éléments max de la liste contenant les status arclan. 
loops = 6

# Temps de pause entre les interrogations Arclan. 
time_sleep = 7 

# => Durée de déclenchement de l'alarme barrière = loops x time_sleep


# Ce status ne sera pas pris en compte si 5x le même dans la liste. 
fence_down = '0010000-0000000000'

debug_in_list = []
debug_out_list = []

while True:
    hook_this("Hello World")
    arc = Arclan()

    arc._arclan_debug_in()
    arc._log_result("INFO", "DEBUG IN "+arc.ip_cac_entree+" "+arc.arclan_in_status)
    debug_in_list.append(arc.arclan_in_status)

    if len(debug_in_list) == loops:
        result = all(elem == debug_in_list[0] for elem in debug_in_list )
        if result and debug_in_list[0] != fence_down:
            print('la liste n est composee que des valeurs IN: '+debug_in_list[0])

            arc._log_result('ERROR', 'Etat barrière borne d entree suspect debug : '+arc.arclan_in_status)
        debug_in_list = []
        
    
    arc._arclan_debug_out()
    arc._log_result("INFO", "DEBUG OUT "+arc.ip_cac_sortie+" "+arc.arclan_out_status)
    debug_out_list.append(arc.arclan_out_status)

    if len(debug_out_list) == loops:
        result = all(elem == debug_out_list[0] for elem in debug_out_list )
        if result and debug_out_list[0] != fence_down:
            print('la liste n est composee que des valeurs IN: '+debug_out_list[0])
            arc._log_result('ERROR', 'Etat barrière borne de sortie suspect debug: '+arc.arclan_out_status)
        debug_out_list = []


    time.sleep(time_sleep)



