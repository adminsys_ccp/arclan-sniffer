FROM python:3

WORKDIR /usr/src/app

ENV TZ="Europe/Paris"

COPY requirements.txt ./

RUN pip install --upgrade pip
#RUN pip install httplib2

RUN pip install --no-cache-dir -r requirements.txt

RUN  pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

RUN mkdir /var/log/arclan-log/

COPY . .

CMD [ "python", "./main.py" ]
